package com.javarush.task.task05.task0504;


/* 
Трикотаж
*/

public class Solution {
    public static void main(String[] args) {
       Cat keny = new Cat ("Keny", 3, 30, 35);
        Cat busia = new Cat("Busia", 2,35,24);
        Cat odin = new Cat("Odin", 2,34,28);

        //напишите тут ваш код
    }

    public static class Cat {
        private String name;
        private int age;
        private int weight;
        private int strength;

        public Cat(String name, int age, int weight, int strength) {
            this.name = name;
            this.age = age;
            this.weight = weight;
            this.strength = strength;
        }

    }
}