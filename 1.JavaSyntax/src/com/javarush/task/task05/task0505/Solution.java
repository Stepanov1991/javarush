package com.javarush.task.task05.task0505;

/* 
Кошачья бойня
*/

public class Solution {

    public static void main(String[] args) {
        Cat kenny = new Cat("Kenny", 4, 40, 40);
        Cat odin = new Cat("Odin", 2, 35, 35);
        Cat bysia = new Cat("Bysia", 2, 35, 45);
     //   kenny.fight(bysia);
        System.out.println("Победитель: " + kenny.fight(bysia));
     //   kenny.fight(odin);
        System.out.println(kenny.fight(odin));
      //  bysia.fight(odin);
        System.out.println(bysia.fight(odin));

        //напишите тут ваш код
    }

    public static class Cat {

        protected String name;
        protected int age;
        protected int weight;
        protected int strength;

        public Cat(String name, int age, int weight, int strength) {
            this.name = name;
            this.age = age;
            this.weight = weight;
            this.strength = strength;
        }

        public boolean fight(Cat anotherCat) {
            int ageScore = Integer.compare(this.age, anotherCat.age);
            int weightScore = Integer.compare(this.weight, anotherCat.weight);
            int strengthScore = Integer.compare(this.strength, anotherCat.strength);

            int score = ageScore + weightScore + strengthScore;
            return score > 0; // return score > 0 ? true : false;
        }
    }
}
