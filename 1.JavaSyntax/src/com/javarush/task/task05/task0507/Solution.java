package com.javarush.task.task05.task0507;

import java.util.Scanner;
/* 
Среднее арифметическое
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        double sa = 0;
        int n = 0;
        double sum = 0;
        while (true) {
            int number = scanner.nextInt();
            if (number == -1)
                break;
            else
                sum = sum + number;
            n++;
            //напишите тут ваш код
        }


        sa = sum / n;
        System.out.println(sa);    //напишите тут ваш код
    }
}

