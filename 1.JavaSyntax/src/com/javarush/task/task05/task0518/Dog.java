package com.javarush.task.task05.task0518;

/* 
Регистрируем собачек
*/


public class Dog {
    String name;
    int height;
    String color;

    public Dog (String name)
    {
        this.name = name;
        this.height = 0;
        this.color = null;
    }
    public Dog (String name, int height)
    {
        this.name = name;
        this.height = height;
        this.color = null;
    }
    public Dog (String name, int height, String color)
    {
        this.name = name;
        this.height = height;
        this.color = color;
    }
    //напишите тут ваш код

    public static void main(String[] args) {

    }
}
