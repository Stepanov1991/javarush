package com.javarush.task.task05.task0529;

import java.util.Scanner;
/* 
Консоль-копилка
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int sum = 0;
        while (true) {
            String s = sc.nextLine();
            if (s.equals("сумма"))
                break;
            else {
                int a = Integer.parseInt(s);
                sum += a;
            }
        }
        System.out.println(sum);
        //напишите тут ваш код
    }
}
