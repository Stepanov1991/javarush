package com.javarush.task.task06.task0605;
import java.util.Scanner;

/* 
Контролируем массу тела
*/

public class Solution {

    public static void main(String[] args) {
     Scanner scanner = new Scanner(System.in);
        double weight = scanner.nextDouble();
        double height = scanner.nextDouble();

        Body.calculateMassIndex(weight, height);
    }

    public static class Body {
        public static void calculateMassIndex(double weight, double height) {
         double vaga = (weight / (height * height) );
         if (vaga<18.5)
             System.out.println("Недовес: меньше чем 18.5");
         else if ((vaga>=18.5) &&(vaga<25 ))
             System.out.println("Нормальный: между 18.5 и 25");
         else if ((vaga>=25) &&(vaga<30 ))
             System.out.println("Избыточный вес: между 25 и 30");
         else
             System.out.println("Ожирение: 30 или больше");

             // напишите тут ваш код
        }
    }
}
