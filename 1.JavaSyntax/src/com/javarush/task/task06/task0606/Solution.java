package com.javarush.task.task06.task0606;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.function.Consumer;
/* 
Чётные и нечётные циферки
*/

public class Solution {

    public static int even;
    public static int odd;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        for (int i = sc.nextInt(); i != 0; i /= 10) {
            int c = i % 10;

            if ((c % 2) == 0)
                even++;
            else
                odd++;
        }



        System.out.println("Even: " + even + " Odd: " + odd);
        //напишите тут ваш код
    }
}
