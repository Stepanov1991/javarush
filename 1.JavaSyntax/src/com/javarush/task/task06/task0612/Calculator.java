package com.javarush.task.task06.task0612;

/* 
Калькулятор
*/

public class Calculator {
    public static int plus(int a, int b) {
        int suma = a + b;
        return  suma;//напишите тут ваш код

    }

    public static int minus(int a, int b) {
        int riznutsia = a - b;//напишите тут ваш код
        return riznutsia;
    }

    public static int multiply(int a, int b) {
        int dobutok = a * b; //напишите тут ваш код
        return dobutok;
    }

    public static double division(int a, int b) {
        double dilenia = (double) a / (double) b;//напишите тут ваш код
        return dilenia;
    }

    public static double percent(int a, int b) {
        double procent = ((double) a* (double) b)/100;//напишите тут ваш код
        return procent;
    }

    public static void main(String[] args) {

    }
}