package com.javarush.task.task06.task0622;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

/* 
Числа по возрастанию
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int d = scanner.nextInt();
        int e = scanner.nextInt();
        for (int i = -1000; i < 1000; i++) {
            if (i == a) System.out.println(a);
            if (i == b) System.out.println(b);
            if (i == c) System.out.println(c);
            if (i == d) System.out.println(d);
            if (i == e) System.out.println(e);
        }
        //напишите тут ваш код
    }
}
