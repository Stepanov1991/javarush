package com.javarush.task.task07.task0701;
import java.util.Scanner;
import java.io.IOException;

/* 
Массивный максимум
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        int[] array = initializeArray();
        int max = max(array);
        System.out.println(max);
    }

    public static int[] initializeArray() throws IOException {
      Scanner scanner = new Scanner(System.in);
int[] list = new int[20];
for (int i = 0; i< list.length; i++)
    list[i] = scanner.nextInt();

      // создай и заполни массив
        return list;
    }

    public static int max(int[] array) {

        int max = array[0];
        for (int i = 1; i < array.length; i++)
        {
            if (array[i] > max)
                max = array[i];
        }
        // найди максимальное значение
        return max;
    }
}
