package com.javarush.task.task07.task0706;

import java.util.Scanner;
import java.io.IOException;

/* 
Улицы и дома
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        int[] numbers = new int[15];
        int sumpar = 0;
        int sumnepar = 0;
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = scanner.nextInt();
            if (i%2==0)
                sumpar+= numbers[i];
            else
                sumnepar+= numbers[i];
        }
        if (sumpar>sumnepar)
            System.out.println("В домах с четными номерами проживает больше жителей.");
        else
            System.out.println("В домах с нечетными номерами проживает больше жителей.");

        //напишите тут ваш код
    }
}
