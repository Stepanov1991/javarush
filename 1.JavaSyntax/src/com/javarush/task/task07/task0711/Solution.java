package com.javarush.task.task07.task0711;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
/* 
Удалить и вставить
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(scanner.nextLine());//напишите тут ваш код
        }
        for (int j = 0; j < 13; j++) {
            list.add(0,list.get(list.size() - 1));
            list.remove(list.size() - 1);
        }
        for (int q = 0; q<list.size(); q++){
            System.out.println(list.get(q));
        }
    }
}
