package com.javarush.task.task07.task0714;

import java.util.ArrayList;
import java.util.Scanner;
/* 
Слова в обратном порядке
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) list.add(scanner.nextLine());
        list.remove(2);
        for (int i = 0; i < list.size(); i++) {
            int j = list.size();
            System.out.println(list.get((j - i -1)));
        }
        //напишите тут ваш код
    }
}
