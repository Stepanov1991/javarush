package com.javarush.task.task07.task0718;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.IOException;

/* 
Проверка на упорядоченность
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) list.add(scanner.nextLine());

        //int y = list.get(0).length();
        for (int i = 1; i < list.size(); ) {
            if (list.get(i).length() > list.get(i - 1).length()) {
                i++;
            } else {

                System.out.print(i);
                break;
            }
        }
    }
}

