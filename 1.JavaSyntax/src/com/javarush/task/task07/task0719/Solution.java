package com.javarush.task.task07.task0719;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Вывести числа в обратном порядке
*/

public class Solution {
    public static void main(String[] args) throws IOException {
    //    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
Scanner scanner = new Scanner(System.in);
ArrayList<Integer> list = new ArrayList<>();
for (int i = 0; i<10; i++) list.add(scanner.nextInt());
for( int i = list.size() - 1; i!=-1; i--) System.out.println(list.get(i));

        //напишите тут ваш код
    }
}
