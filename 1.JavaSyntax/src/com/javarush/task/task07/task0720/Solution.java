package com.javarush.task.task07.task0720;

import java.util.ArrayList;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Перестановочка подоспела
*/

public class Solution {
    public static void main(String[] args) throws IOException {
             BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> list = new ArrayList<>();
        //Scanner scanner = new Scanner(System.in);
        int N = Integer.parseInt(reader.readLine());
        int M = Integer.parseInt(reader.readLine());
        for (int i = 0; i < N; i++) {
            String s = reader.readLine();
            list.add(s);
        }
        for (int i = 0; i < M; i++) {
          list.add(list.remove(0));
        }


        for (int i = 0; i < list.size(); i++) System.out.println(list.get(i));

        //напишите тут ваш код
    }
}
