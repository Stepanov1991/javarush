package com.javarush.task.task07.task0724;

/* 
Семейная перепись
*/

public class Solution {
    public static void main(String[] args) {
     Human Ivan = new Human("Ivan",true, 79);
        Human Ivana = new Human("Ivana",false,77);
        Human Vitia = new Human("Vitia",true,84);
        Human Viktoria = new Human("Viktoria", false, 80);
        Human Dima = new Human("Dima", true, 55,Ivan,Ivana);
        Human Sveta = new Human("Sveta", false, 50,Vitia,Viktoria);
        Human Andrey = new Human("Andrey", true, 28,Dima,Sveta);
        Human Natasha = new Human("Natash", false, 27,Dima,Sveta);
        Human Yra = new Human("Yra", true, 25,Dima,Sveta);

        System.out.println(Ivan.toString());
        System.out.println(Ivana.toString());
        System.out.println(Vitia.toString());
        System.out.println(Viktoria.toString());
        System.out.println(Dima.toString());
        System.out.println(Sveta.toString());
        System.out.println(Andrey.toString());
        System.out.println(Natasha.toString());
        System.out.println(Yra.toString());
     // напишите тут ваш код
    }

    public static class Human {
        String name;
        boolean sex;
        int age;
        Human father;
        Human mother;

        public Human(String name, boolean sex, int age, Human father, Human mother) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = father;
            this.mother = mother;

        }

        public Human(String name, boolean sex, int age) {
            this.name = name;
            this.sex = sex;
            this.age = age;
        }

        // напишите тут ваш код

        public String toString() {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null) {
                text += ", отец: " + this.father.name;
            }

            if (this.mother != null) {
                text += ", мать: " + this.mother.name;
            }

            return text;
        }
    }
}